Projet Dana: Open Data in the Cloud of Linked Data
==================================================

<p>L’objectif du projet est de transformer les données ouvertes de votre choix en données sémantiques et de lier ses données sémantiques au cloud de "Linked Data"</p>

Source de données :
-------------------

Lien : [L’agenda des événements de la Ville de Nantes et de Nantes Métropole]( https://data.nantesmetropole.fr/explore/dataset/244400404_agenda-evenements-nantes-nantes-metropole/table/?disjunctive.emetteur&disjunctive.rubrique&disjunctive.lieu&disjunctive.ville) </p>

Description des fichiers : 
--------------------------

* ConstructAgenda.sparql  => Fichier contenant la requête sparql qui permet de transformer les données qui sont en .csv en ttl.

* donneAgenda.ttl ==> Fichier contenant les données de l’agenda des événements de la Ville de Nantes et de Nantes Métropole en ttl.

* requete1.rq ==> Requête qui permet de sélectionner le nom, l’heure de début et l’heure de fin des événements qui auront lieu à Nantes.

* requete2.rq ==>  Requête qui permet sélectionné les événements sportif qui auront lieu à Nantes. (En se limitant au 10 premiers).

* donneeParking.ttl ==> représente les données des parking public à Nantes.

* requete3.ttl ==> Requête qui permet de lier entre les deux fichiers ttl et qui permet de  sélectionner tous les événements sportifs avec le nom, l’adresse, code postale 
                et la capacité de stationnement handicap du parking, qui est bien sûr dans la même commune que l'événement.


<p> Pour plus d'informations concernant le projet (Résultats des requêtes , Utilisation des vocabulaires VOID pour décrire nos datasets , et une description plus détaillé du projet), merci bien de
visiter le diapo sur le lien suivant : [Diapo](https://docs.google.com/presentation/d/1h5xC734PQuMCJvuoNiWNXR975fkxqWnUPq43Kwf5HUg/edit#slide=id.p) </p>